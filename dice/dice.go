package dice

import (
	"math/rand"
	"time"
)

func RollDie(max int) int {
	return rand.Intn(max) + 1
}

type RollResult struct {
	Total int
	Rolls []int
}

func Roll(num int, max int) RollResult {
	total, rolls := 0, make([]int, num)
	for i := 0; i < num; i++ {
		rand.Seed(time.Now().UnixNano() + int64(i))
		rolls[i] = RollDie(max)
		total += rolls[i]
	}
	return RollResult{Rolls: rolls, Total: total}
}
