/*----------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for license information.
 *---------------------------------------------------------------------------------------*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/microsoft/vscode-remote-try-go/dice"
)

type Error struct {
	Message string
}

func handle(w http.ResponseWriter, r *http.Request) {
	num := r.URL.Query().Get("num")
	max := r.URL.Query().Get("max")

	numDice, numErr := strconv.Atoi(num)
	maxDice, maxErr := strconv.Atoi(max)

	if numErr != nil || maxErr != nil {
		json.NewEncoder(w).Encode(Error{Message: "error converting string to int"})
		return
	}

	var result dice.RollResult = dice.Roll(numDice, maxDice)
	json.NewEncoder(w).Encode(result)
}

func main() {
	portNumber := "9000"
	http.HandleFunc("/", handle)
	fmt.Println("Server listening on port ", portNumber)
	http.ListenAndServe(":"+portNumber, nil)
}
